FROM gradle:7.1-jdk16 AS build
WORKDIR /socrates

COPY ./gradle/             /socrates/gradle
COPY ./build.gradle.kts    /socrates/build.gradle.kts
COPY ./settings.gradle.kts /socrates/settings.gradle.kts
COPY ./gradle.properties   /socrates/gradle.properties
COPY ./gradlew             /socrates/gradlew
COPY ./src                 /socrates/src

RUN ./gradlew shadowJar --no-daemon

FROM openjdk:11.0.8-jre-slim
WORKDIR /workspace

COPY --from=build /socrates/build/libs/bot.jar /workspace/bot.jar

ENTRYPOINT ["java", "-jar", "/workspace/bot.jar"]