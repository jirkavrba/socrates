package dev.vrba.discord.socrates.commands

import me.jakejmattson.discordkt.arguments.AnyArg
import me.jakejmattson.discordkt.commands.commands


@Suppress("Unused", "DestructuringWrongName")
fun choose() = commands("Choose") {
    slash(name = "choose", description = "Choose one of the provided answers") {
        execute(
            AnyArg(name = "question", description = "The question that you want to know the answer to"),
            AnyArg(name = "first_answer", description = "First answer"),
            AnyArg(name = "second_answer", description = "Second answer"),
            AnyArg(name = "third_answer", description = "Third answer").optionalNullable(),
            AnyArg(name = "fourth_answer", description = "Fourth answer").optionalNullable(),
        ) {
            val (question, first, second, third, fourth) = args
            val answer = listOfNotNull(first, second, third, fourth).random()

            respondPublic {
                title = question
                description = "I've chosen: **${answer}**"
                image = "https://i.imgur.com/UzBcuZS.png"
            }
        }
    }
}