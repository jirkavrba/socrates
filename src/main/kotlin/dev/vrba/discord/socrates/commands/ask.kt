
package dev.vrba.discord.socrates.commands

import me.jakejmattson.discordkt.arguments.AnyArg
import me.jakejmattson.discordkt.commands.commands

@Suppress("unused")
fun ask() = commands("Ask") {
    slash(name = "ask", description = "Ask me any question you want") {
        execute(AnyArg(name = "question", description = "The question that you want to ask me")) {
            val (question) = args
            val response = setOf(
                "No",
                "Yes",
                "It depends",
                "Indeed",
                "I'm disappointed",
                "Does it really matter?",
                "Just let go",
            ).random()

            respondPublic {
                title = question
                description = response
                image = "https://i.imgur.com/GHXdh6v.png"
            }
        }
    }
}