package dev.vrba.discord.socrates

import dev.kord.common.annotation.KordPreview
import dev.kord.gateway.Intents
import me.jakejmattson.discordkt.dsl.bot

@OptIn(KordPreview::class)
fun main() {
    val token = System.getenv("DISCORD_TOKEN") ?: throw RuntimeException("DISCORD_TOKEN is not configured properly!")

    bot(token) {
        configure {
            intents = Intents.nonPrivileged
        }
    }
}