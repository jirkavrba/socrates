plugins {
    kotlin("jvm") version "1.9.0"
    id("com.github.johnrengelman.shadow") version "7.0.0"
}

group = "dev.vrba.discord"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("me.jakejmattson:DiscordKt:0.23.4")
    testImplementation(kotlin("test"))
}

tasks {
    test {
        useJUnitPlatform()
    }

    shadowJar {
        archiveFileName.set("bot.jar")
        manifest {
            attributes("Main-Class" to "dev.vrba.discord.socrates.SocratesKt")
        }
    }
}

kotlin {
    jvmToolchain(8)
}