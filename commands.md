# Commands

## Key 
| Symbol      | Meaning                        |
|-------------|--------------------------------|
| [Argument]  | Argument is not required.      |

## Ask
| Commands | Arguments | Description                  |
|----------|-----------|------------------------------|
| ask      | question  | Ask me any question you want |

## Choose
| Commands | Arguments                                                              | Description                        |
|----------|------------------------------------------------------------------------|------------------------------------|
| choose   | question, first_answer, second_answer, [third_answer], [fourth_answer] | Choose one of the provided answers |

## Utility
| Commands | Arguments | Description           |
|----------|-----------|-----------------------|
| Help     | [Command] | Display a help menu.  |
| info     |           | Bot info for Socrates |

